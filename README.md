# Udp sockets (C language)

Based on article [Programming UDP sockets in C on Linux – Client and Server example](https://www.binarytides.com/programming-udp-sockets-c-linux/)

# Task
Client write a number which sends to a server. Server read number and print it in several 
number systems (dec, hex, bits, oct, custom)

# Task (RU)
Создать клиент-серверное приложение на основе сетевых (системных) сокетов, транспортный протокол UDP. Использовать язык С.
1. Клиент должен передавать серверу десятичное число (вводится с клавиатуры).
2. Число отправляется на сервер.
3. На сервере число выводится в десятичной, двоичной, шестнадцатеричной, восьмеричной системах счисления + системе счисления с основанием номер студента по списку.
Сервер и клиент должны корректно обрабатывать начало и конец соединения.