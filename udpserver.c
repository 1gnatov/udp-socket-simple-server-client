/*
	Simple udp server
*/
#include<stdio.h>	//printf
#include<string.h> //memset
#include<stdlib.h> //exit(0);
#include<arpa/inet.h>
#include<sys/socket.h>
#include<unistd.h> // for close

#define BUFLEN 512	//Max length of buffer
#define PORT 8888	//The port on which to listen for incoming data

void processClientNumber(int clientNumber, char *buffer);
char *itoa(int v, char *s, int rad);

void die(char *s)
{
	perror(s);
	exit(1);
}

int main(void)
{
	struct sockaddr_in si_me, si_other;
	
	int s, i, slen = sizeof(si_other) , recv_len;
	char buf[BUFLEN];
	
	//create a UDP socket
	if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
	{
		die("socket");
	}
	
	// zero out the structure
	memset((char *) &si_me, 0, sizeof(si_me));
	
	si_me.sin_family = AF_INET;
	si_me.sin_port = htons(PORT);
	si_me.sin_addr.s_addr = htonl(INADDR_ANY);
	
	//bind socket to port
	if( bind(s , (struct sockaddr*)&si_me, sizeof(si_me) ) == -1)
	{
		die("bind");
	}

	char *numbBuf = calloc(1024, sizeof(char));
	
	//keep listening for data
	while(1)
	{
		printf("Waiting for data...");
		fflush(stdout);
		
		//try to receive some data, this is a blocking call
		if ((recv_len = recvfrom(s, buf, BUFLEN, 0, (struct sockaddr *) &si_other, &slen)) == -1)
		{
			die("recvfrom()");
		}
		
		//print details of the client/peer and the data received
		printf("Received packet from %s:%d\n", inet_ntoa(si_other.sin_addr), ntohs(si_other.sin_port));
		int clientNumber = atoi(buf);
		processClientNumber(clientNumber, numbBuf);
		
		// //now reply the client with the same data
		// if (sendto(s, buf, recv_len, 0, (struct sockaddr*) &si_other, slen) == -1)
		// {
		// 	die("sendto()");
		// }
	}

	close(s);
	return 0;
}

void processClientNumber(int clientNumber, char *buffer)
{
    printf("Number in numberal system 10: %d\n", clientNumber);
    printf("Number in numberal system 2: %s\n", itoa(clientNumber, buffer, 2));
    printf("Number in numberal system 16: %s\n", itoa(clientNumber, buffer, 16));
    printf("Number in numberal system 8: %s\n", itoa(clientNumber, buffer, 8));
    printf("Number in numberal system %d: %s\n", 7, itoa(clientNumber, buffer, 7));
}

char *itoa(int v, char *s, int rad)
{
    int k = 0;
    while (v)
    {
        int x = v % rad;
        s[k++] = (x < 10) ? '0' + x : 'A' + x - 10;
        v /= rad;
    }
    s[k] = 0;
    for (int i = 0; i < k / 2; i++)
    {
        int t = s[i];
        s[i] = s[k - i - 1];
        s[k - i - 1] = t;
    }
    return s;
}
